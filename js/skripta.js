$(document).ready(function() {
    var baseUrl = "https://chat.data-lab.si/api";
    var user = {id: 69, name: "Ace Pece"}; //TODO: vnesi svoje podatke
    var nextMessageId = 0;
    var currentRoom = "Skedenj";

    //Naloži seznam sob
    $.ajax({
        url: baseUrl + "/rooms",
        type: "GET",
        success: function (data) {
            for (i in data) {
                $("#rooms").append(" \
                    <li class='media'> \
                        <div class='media-body room' style='cursor: pointer;'> \
                            <div class='media'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                                </a> \
                                <div class='media-body'> \
                                    <h5>" + data[i] + "</h5> \
                                </div> \
                            </div> \
                        </div> \
                    </li>");
            }
            $(".room").click(spremembaSobe);
        }
    });

    //TODO: Naloga
    //Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund
    pridobivanjePogovorov = function(){
      $.ajax({
          url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
          type: "GET",
          success: function(data) {
              for(i in data) {
                  var message = data[i];
                  $("#messages").append(" \
                    <li class='media'> \
                        <div class='media-body'> \
                            <div class='media'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle'src='https://randomuser.me/api/portraits/men/"+message.user.id + ".jpg' /> \
                                    </a> \
                                    <div class='media-body'> \
                                        <small class='text-muted'>" + message.user.name + " | " +message.time + "</small><br/>" + message.text + " \
                                        <hr/> \
                                        </div> \
                                        </div> \
                                        </div> \
                                        </li>");
                                        nextMessageId = message.id+1;
              }
              setTimeout(function(){pridobivanjePogovorov()},5000);
          }
      })  
    }
    
    //Klicanje funkcije za začetek nalaganja pogovorov
    $("#messages").html("");
    pridobivanjePogovorov();
    
    //TODO: Naloga
    //Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
    posodobiUporabnike = function() {
        $.ajax ({
            url: baseUrl + "/users/" + currentRoom,
            type: "GET",
            success: function(data){
                $("#users").html("");
                for(i in data){
                    var user = data[i];
                    $("#users").append(" \
                    <li class='media'> \
                     <div class='media-body'> \
                      <div class='media'> \
                       <a class='pull-left' href='#'> \
                         <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/"+user.id + ".jpg' /> \
                         </a> \
                         <div class='media-body'> \
                          <h5>" + user.name + "</h5> \
                         </div> \
                      </div> \
                     </div> \
                    </li>");
                }
                setTimeout(function() {posodobiUporabnike()}, 5000);
            }
        })
    }
    //Klicanje funkcije za začetek posodabljanja uporabnikov
    posodobiUporabnike();
    
    //TODO: Naloga
    //Definicija funkcije za pošiljanje sporočila
    posljiSporocilo = function() {
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom,
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({user: user, text: $("#message").val()}),
            success: function(data) {
                $("#message").val("");
            },
            error: function(err){
                alert("prislo je do napake pri pošiljanje sporočila. Prosim poskusite znova!");
            }  
        })
    }
    
    //On Click handler za pošiljanje sporočila
    $("#send").click(posljiSporocilo);

    //TODO: Naloga
    //Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
    //V razmislek: pomisli o morebitnih težavah!
    spremembaSobe = function(event){
        $("#messages").html("");
        $("#users").html("");
        currentRoom = event.currentTarget.getElementsByTagName("h5")[0].innerHTML;
        nextMessageId = 0;
    }
    
    //On Click handler za menjavo sobe
    //Namig: Seznam sob se lahko naloži kasneje, kot koda, ki se izvede tu.
});